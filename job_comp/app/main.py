"""
This is a sample hello world app
It prints hello world 100 times
"""
__author__ = "Naveen Sinha"

import time
print "Hello World"

if __name__ == '__main__':

    ### $xpr_param_job_import

  COUNT_TIMES = 100
  while COUNT_TIMES:
    print(COUNT_TIMES)
    COUNT_TIMES-=1
    time.sleep(1)
